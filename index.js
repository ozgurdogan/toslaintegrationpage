var backend = " https://toslachatsortest.akbank.com/";
var params = getUrlParams();
console.log("params", params);
getKeyValue(params.clientID, "toslaID", params.token, function(toslaID) {
  openIframe(toslaID, params.token);
});
function getUrlParams() {
  const list = window.location.search.substring(1).split("&");
  var m = {};
  for (var i = 0; i < list.length; i++) {
    var indexOf = list[i].indexOf("=");
    if (indexOf >= 0)
      m[list[i].substring(0, indexOf)] = decodeURIComponent(
        list[i].substring(indexOf + 1)
      );
  }
  return m;
}
function getKeyValue(clientID, key, token, callback) {
  var url = backend + "ClientKeyValue?token=" + token + "&id=" + clientID;
  getUrl(url, function(data) {
    if (
      data === undefined ||
      data.content === undefined ||
      data.content.length === 0
    ) {
      return console.error("Error HTTP CALL");
    }

    for (var i = 0; i < data.content.length; i++) {
        console.log('key-value',data.content[i].k,data.content[i].v);
      if (data.content[i].k === key) {
        console.log("getKeyValue", data.content[i].v);
        callback(data.content[i].v);
        break;
      }
    }
    console.log("getKeyValue", undefined);
    callback("undefined");
  });
}
function getUrl(url, callback) {
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      var data = JSON.parse(request.responseText);
      callback(data);
    } else {
      console.log("Error 1");
    }
  };

  request.onerror = function() {
    console.log("Error 2");
  };

  request.send();
}
function postUrl(url) {
  var request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader(
    "Content-Type",
    "application/x-www-form-urlencoded; charset=UTF-8"
  );
  request.send("");
}
function openIframe(toslaID, token) {
  var url =
    "https://direktbackendwebint.akbank.com/Toslabackoffice/?owner=sorun&sorunCustomerId=";
  url += toslaID;
  url += "&token=";
  url += token;
  console.log("openIframe", url);
  document.getElementById("iframe").src = url;
}

function callNumber(number) {
  console.log("callNumber", number);
  // Kabuk call number fonksiyonu
}

function setToslaID(toslaID, clientID, token) {
  console.log("setToslaID", toslaID);
  var url = backend + "ClientKeyValue?token=";
  url += token;
  url += "&id=";
  url += clientID;
  url += "&key=toslaID&value=";
  url += toslaID;
  postUrl(url, function() {
    console.log("Tosla ID set");
  });
}
